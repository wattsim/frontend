FROM nginx:alpine
LABEL maintainer="Erwan Rouchet <lucidiot@protonmail.com>"
EXPOSE 80

RUN rm /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/conf.d/frontend.conf
COPY dist /frontend

CMD ["nginx", "-g", "daemon off;"]
